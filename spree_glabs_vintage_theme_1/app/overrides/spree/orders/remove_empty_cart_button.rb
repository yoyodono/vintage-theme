Deface::Override.new(:virtual_path     => "spree/orders/edit",
                     :replace_contents => "div#empty-cart",
                     :name             => "remove_empty_cart_button",
                     :partial          => "spree/orders/new_empty_cart_div")
