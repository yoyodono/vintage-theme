Deface::Override.new(:virtual_path  => "spree/shared/_footer",
                     :insert_before => "footer#footer",
                     :name          => "insert_fat_footer",
                     :partial       => "spree/shared/fat_footer")

Deface::Override.new(:virtual_path     => "spree/shared/_footer",
                     :replace_contents => "footer#footer",
                     :name             => "footer_company_name",
                     :partial          => "spree/shared/footer_company_name")
