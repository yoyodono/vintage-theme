module Spree
  UserSessionsController.class_eval do
    helper "Spree::Products"
    include Spree::SpreeVintageTheme::InfosHelper
    before_filter :new_variable
    def new_variable
      @current_theme_color = @current_theme_color.nil? ? "default.css" : @current_theme_color
      @menus = Spree::Taxon.all.select{ |t| !t.parent_id.nil? }.slice(0,5)
      @list_locales = Config.built_in_locales

      load_new_variable
    end
  end
end
