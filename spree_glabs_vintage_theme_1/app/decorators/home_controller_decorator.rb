module Spree
  HomeController.class_eval do
    include Spree::Core::ControllerHelpers::Order
    before_filter :get_list_products, :only => :index
    def get_list_products
      arr = []
      Spree::Product.all.each do |product|
        variants = product.variants_including_master_and_deleted
        line_item = Spree::LineItem.find(:all, :conditions => ["variant_id IN (?)", variants ] )
        quantities = line_item.map(&:quantity)
        quantity_total = quantities.inject{|sum,x| sum + x}.nil? ? 0 : quantities.inject{|sum,x| sum + x}
        arr.push({:product_id => product.id, :quantity => quantity_total})
      end
      @list_best_products = arr.sort {|a, b| b[:quantity] <=> a[:quantity]}.slice!(0, 1)
      @list_best_products = Spree::Product.find(:all, :conditions => ["id IN (?)", @list_best_products.collect{ |p| p[:product_id] }])
      if @list_best_products.nil?
        @list_best_products = Spree::Product.limit(1)
      end

      @list_new_products = Spree::Product.order("created_at DESC").limit(1)
      if @list_new_products.nil?
        @list_new_products = Spree::Product.limit(1)
      end
      @list_promotion_products = []
      Spree::Promotion::Rules::Product.all.each do |promotion|
        promotion.products.each do |product|
          @list_promotion_products.push(product)
        end
      end
      if @list_promotion_products.nil?
        @list_promotion_products = Spree::Product.limit(1)
      end
    end
  end
end
