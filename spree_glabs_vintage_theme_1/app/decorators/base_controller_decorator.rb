module Spree
  BaseController.class_eval do
    include Spree::Core::ControllerHelpers::Order
    include Spree::SpreeVintageTheme::InfosHelper
    before_filter :get_slider_images

    def get_slider_images
      @slider_images = Spree::SpreeVintageTheme::ThemeImage.where(:enabled => "true")
      @current_theme_color = @current_theme_color.nil? ? "default.css" : @current_theme_color

      @current_order_user = current_order(true)
      @menus = Spree::Taxon.all.select{ |t| !t.parent_id.nil? }.slice(0,5)
      @list_locales = Config.built_in_locales

      load_new_variable
    end
  end
end
